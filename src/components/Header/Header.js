import React from 'react';
import {NavLink} from 'react-router-dom';
import './Header.css';

const Header = () => {
    return(
        <header>
            <h2>
                <NavLink to="/">Quotes Central</NavLink>
            </h2>

            <ul>
                <li>
                    <NavLink to="/">Quotes</NavLink>
                </li>
                <li>
                    <NavLink to="/add-quote">Submit new quote</NavLink>
                </li>
            </ul>
        </header>
    )
};

export default Header;