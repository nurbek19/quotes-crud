import React from 'react';
import {Link} from 'react-router-dom';

const Category = props => {
    let route = '';

    if(props.id === '/') {
        route = '/';
    } else {
        route = /quotes/ + props.id;
    }

    return (
        <li>
            <Link to={route}>{props.category}</Link>
        </li>
    )
};

export default Category;