import React from 'react';
import {Link} from 'react-router-dom';
import './Quote.css';

const Quote = props => {
    return(
        <div className="quote">
            <div className="actions">
                <Link to={`/quotes/${props.id}/edit`}>Edit</Link>
                <button onClick={props.remove}>Delete</button>
            </div>
            <h3>{props.body}</h3>
            <p>{props.author}</p>
        </div>
    )
};

export default Quote;