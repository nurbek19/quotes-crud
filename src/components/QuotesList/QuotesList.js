import React from 'react';
import Quote from './Quote/Quote';
import './QuotesList.css';

const QuotesList = props => {
    let quotes;

    if (props.quotes.length === 0) {
        quotes = <p>No quotes in this category!</p>
    } else {
        quotes = <div className="quotes">

            {props.quotes.map(quote => {
                return <Quote
                    author={quote.author}
                    body={quote.body}
                    key={quote.id}
                    id={quote.id}
                    remove={() => props.remove(quote.id)}
                />
            })}
        </div>;
    }

    return (
        <div className="quotes-list">
            <h2>{props.title}</h2>
            {quotes}
        </div>
    );
};

export default QuotesList;