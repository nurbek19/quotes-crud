import React from 'react';
import './QuoteForm.css';

const QuoteForm = props => {
    return (
        <form action="#" className="quote-form">
            <legend>{props.title}</legend>

            <label htmlFor="category">Category

                <select name="category" id="type"
                        onChange={props.changed}
                        value={props.category}
                >
                    <option value="null" disabled="true">Choose category</option>
                    <option value="star-wars">Star Wars</option>
                    <option value="motivational">Motivational</option>
                    <option value="famous-people">Famous People</option>
                    <option value="humour">Humour</option>
                    <option value="saying">Saying</option>
                </select>
            </label>

            <label htmlFor="autor">Author
                <input type="text" name="author"
                       id="author"
                       value={props.author}
                       onChange={props.changed}
                />
            </label>

            <label htmlFor="body">Quote text
                <textarea name="body" id="body"
                          cols="30" rows="10"
                          value={props.body}
                          onChange={props.changed}
                />
            </label>

            <button onClick={props.createQuote}>Save</button>
        </form>
    )
};

export default QuoteForm;