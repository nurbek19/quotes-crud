import React, {Component, Fragment} from 'react';
import {Switch, Route} from 'react-router-dom';
import Header from './components/Header/Header';
import './App.css';
import HomePage from "./containers/HomePage/HomePage";
import AddQuotes from "./containers/AddQuotes/AddQuotes";
import CategoryPage from "./containers/CategoryPage/CategoryPage";
import EditQuote from "./containers/EditQuote/EditQuote";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Header />

                <Switch>
                    <Route path="/" exact component={HomePage} />
                    <Route path="/add-quote" component={AddQuotes} />
                    <Route path="/quotes/:id" exact component={CategoryPage} />
                    <Route path="/quotes/:id/edit" component={EditQuote} />
                </Switch>
            </Fragment>
        );
    }
}

export default App;
