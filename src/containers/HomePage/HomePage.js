import React, {Component} from 'react';
import Categories from '../../containers/Categories/Categories';
import QuotesList from '../../components/QuotesList/QuotesList';
import axios from 'axios';
import './HomePage.css';

class HomePage extends Component {
    state = {
        quotes: [],
        loading: false
    };

    componentDidMount() {
        this.setState({loading: true});

        axios.get('quotes.json').then(response => {

            const quotes = [];

            for (let key in response.data) {
                quotes.unshift({...response.data[key], id: key});
            }
            this.setState({quotes, loading: false});
        });
    }

    removeQuote = id => {
        axios.delete(`quotes/${id}.json`).then(() => {
            this.setState(prevState => {
                const quotes = [...prevState.quotes];
                const index = quotes.findIndex(quote => quote.id === id);
                quotes.splice(index, 1);
                return {quotes};
            })
        });
    };

    render() {
        let quotes = <QuotesList
            title="ALL"
            quotes={this.state.quotes}
            remove={this.removeQuote}
        />;

        if (this.state.loading) {
            quotes = <p style={{fontSize: '24px', textAlign: 'center', paddingTop: '20px', width: '100%'}}>Loading...</p>
        }
        return (
            <div className="home-page">
                <Categories/>
                {quotes}
            </div>
        );
    }
}

export default HomePage;