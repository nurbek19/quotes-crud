import React, {Component} from 'react';
import Category from '../../components/Category/Category';
import './Categories.css';

class Categories extends Component {
    state = {
        categories: [
            {title: 'All', id: '/'},
            {title: 'Star Wars', id: 'star-wars'},
            {title: 'Motivational', id: 'motivational'},
            {title: 'Famous People', id: 'famous-people'},
            {title: 'Humour', id: 'humour'},
            {title: 'Saying', id: 'saying'}
        ]
    };

    render() {
        return (
            <ul className="categories">
                {this.state.categories.map(category => {
                    return <Category key={category.id} id={category.id} category={category.title} />
                })}
            </ul>
        )
    }
}

export default Categories;