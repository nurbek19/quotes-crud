import React, {Component} from 'react';
import QuoteForm from '../../components/QuoteForm/QuoteForm';
import axios from 'axios';

class EditQuote extends Component {

    state = {
        editQuote: null,
        quoteID: ''
    };

    changeValue = event => {
        const name = event.target.name;
        this.setState({editQuote: {...this.state.editQuote, [name]: event.target.value}});
    };

    createQuote = event => {
        event.preventDefault();

        axios.put(`/quotes/${this.state.quoteID}.json`, this.state.editQuote).then(() => {
            this.props.history.replace('/');
        });
    };

    componentDidMount() {
        const id = this.props.match.params.id;

        axios.get(`/quotes/${id}.json`).then(response => {
            this.setState({editQuote: response.data, quoteID: id});
        });
    }

    render() {
        if (this.state.editQuote) {
            return (
                <QuoteForm
                    title="Edit quote"
                    author={this.state.editQuote.author}
                    body={this.state.editQuote.body}
                    category={this.state.editQuote.category}
                    changed={this.changeValue}
                    createQuote={this.createQuote}
                />
            )
        } else {
            return <p>Loading...</p>
        }
    }
}

export default EditQuote;