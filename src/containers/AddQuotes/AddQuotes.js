import React, {Component} from 'react';
import QuoteForm from '../../components/QuoteForm/QuoteForm';
import axios from 'axios';

class AddQuotes extends Component {
    state = {
        editQuote: {
            author: '',
            body: '',
            category: 'null'
        }
    };

    changeValue = event => {
        const name = event.target.name;
        this.setState({editQuote: {...this.state.editQuote, [name]: event.target.value}});
    };

    createQuote = event => {
        event.preventDefault();

        axios.post('quotes.json', this.state.editQuote).then(() => {
            this.props.history.replace('/');
        });
    };

    render() {
        return (
            <QuoteForm
                title="Submit new quote"
                author={this.state.editQuote.author}
                body={this.state.editQuote.body}
                category={this.state.editQuote.category}
                changed={this.changeValue}
                createQuote={this.createQuote}
            />
        )
    }
}

export default AddQuotes;