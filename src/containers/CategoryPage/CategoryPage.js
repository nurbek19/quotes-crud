import React, {Component} from 'react';
import Categories from '../../containers/Categories/Categories';
import QuotesList from '../../components/QuotesList/QuotesList';
import axios from 'axios';
import './CategoryPage.css';

class CategoryPage extends Component {
    state = {
        quotes: [],
        loading: false,
        quotesCategory: ''
    };

    getQuotes = category => {
        axios.get(`quotes.json?orderBy="category"&equalTo="${category}"`).then(response => {

            const quotes = [];

            for (let key in response.data) {
                quotes.unshift({...response.data[key], id: key});
            }
            this.setState({quotes, loading: false, quotesCategory: category});
        });
    };

    componentDidMount() {
        this.setState({loading: true});

        const category = this.props.match.params.id;

        this.getQuotes(category);
    }

    componentDidUpdate() {
        const category = this.props.match.params.id;

        if (this.state.quotesCategory !== category) {
            this.setState({loading: true, quotesCategory: this.props.match.params.id});
            this.getQuotes(category);
        }
    }

    removeQuote = id => {
        axios.delete(`quotes/${id}.json`).then(() => {
            this.setState(prevState => {
                const quotes = [...prevState.quotes];
                const index = quotes.findIndex(quote => quote.id === id);
                quotes.splice(index, 1);
                return {quotes};
            })
        });
    };

    render() {
        let quotes = <QuotesList
            title={this.state.quotesCategory}
            quotes={this.state.quotes}
            remove={this.removeQuote}
        />;

        if (this.state.loading) {
            quotes = <p style={{fontSize: '24px', textAlign: 'center', paddingTop: '20px', width: '100%'}}>Loading...</p>
        }
        return (
            <div className="category-page">
                <Categories/>
                {quotes}
            </div>
        );

    }
}

export default CategoryPage;